# Lesson 1
```
print("hello world")
```

# Lesson 2
New file lesson2.py
```python
 x = input("Bitte einen Text eingeben: ")
print("Ihre Eingabe: ", x)
```
> python3 lesson2.py

Sherzod’s code:
```python
print("Bitte einen Text eingeben")
x = input()
print("Ihre Eingabe:", x)
```

# Datentypes
## Numeric Datentypes
- für ganze Zahlen: int und long
- für Gleitkommazahlen: float
- für komplexe Zahlen: complex
- für boolsche Werte: bool

# Lesson 3
Create a new file 
```python
x = input("Bitte eine Zahl:  ")
# is it a number?
if int(x) % 2 == 0:
  print(" juft ")
else:
  print("toq")
```

# Lesson 4
```python
x = input("Bitte eine Zahl:  ")

if x.isdigit():
    if int(x) % 2 == 0:
      print("gerade")
    else:
      print("ungerade")
else:
  print("nur Zahlen")
```

# Lesson 5 - while
Loops with while
```python
i = 1
while i < 6:
  print(">>>>>>>>>>>>>  ", i)
  if i == 3:
    break
  i += 1
```

# NEW --------
Prepare a proper editor for the next lesson
Visual code:
Windows 10 please execute the following line in the power shell:
choco install vscode
MacOS please execute the following line in the terminal:


# Lesson 5 - list
Datatype for collections in python
```python
comming soon
```
